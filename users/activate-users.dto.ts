import { IsOptional, Length, IsEmail } from 'class-validator'
export const getActivateUserDto = (ApiPropertySwagger?: any) => {
  const ApiProperty = ApiPropertySwagger || function () { }

  class ActivateUserDto {
    @ApiProperty()
    @IsEmail()
    @Length(5, 60)
    @IsOptional()
    email?: string
  }

  return ActivateUserDto
}