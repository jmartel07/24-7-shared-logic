import { Transform } from 'class-transformer'
import { IsOptional, IsDate } from 'class-validator'

export const getDeactivateUserDto = (ApiPropertySwagger?: any, picture_path?: string, picture_url?: string) => {
  const ApiProperty = ApiPropertySwagger || function () { }

  class DeactivateUserDto {
    @IsOptional()
    @ApiProperty({ required: false })
    @Transform(({ value }) => new Date(new Date(value)))
    @IsDate()
    date: Date = new Date(new Date());
  }

  return DeactivateUserDto
}
