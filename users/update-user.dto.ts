import { Transform } from 'class-transformer'
import {
  IsNotEmpty,
  IsEmail,
  IsOptional,
  Length,
  IsEnum,
} from 'class-validator'
import { getUpdateUserMineDto } from './update-user-mine.dto'
import { UserType } from '../constants'
export const getUpdateUserDto = (ApiPropertySwagger?: any) => {
  const ApiProperty = ApiPropertySwagger || function () { }

  const UpdateUserMineDto = getUpdateUserMineDto(ApiProperty)

  class UpdateUserDto extends UpdateUserMineDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    @Length(5, 60)
    email: string

    @ApiProperty({
      required: false,
      enum: UserType,
    })
    @IsOptional()
    @IsEnum(UserType)
    userType: UserType

  }

  return UpdateUserDto
}
