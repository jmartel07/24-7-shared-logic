
import { Transform } from 'class-transformer'
import { Length, Matches } from 'class-validator'
import { getUpdateProfilePictureDto } from './update-profile-picture.dto'

export const getUpdateUserMineDto = (ApiPropertySwagger?: any) => {
  const ApiProperty = ApiPropertySwagger || function () { }

  const UpdateProfilePictureDto = getUpdateProfilePictureDto(ApiProperty)

  class UpdateUserMineDto extends UpdateProfilePictureDto {
    @ApiProperty()
    @Matches(/[a-zA-Z ]/, {
      message: 'first name can only contain letters and spaces.',
    })
    @Transform(({ value }) => value?.toString().trim())
    @Length(3, 60)
    firstName: string

    @ApiProperty()
    @Matches(/[a-zA-Z ]/, {
      message: 'last name can only contain letters and spaces.',
    })
    @Transform(({ value }) => value?.toString().trim())
    @Length(3, 60)
    lastName: string
  }

  return UpdateUserMineDto
}

