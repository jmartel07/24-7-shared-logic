import { getUpdateUserDto } from './update-user.dto'

export const getCreateUserDto = (ApiPropertySwagger?: any, picture_path?: string, picture_url?: string) => {
  const ApiProperty = ApiPropertySwagger || function () { }
  const UpdateUserDto = getUpdateUserDto(ApiProperty)

  class CreateUserDto extends UpdateUserDto { }

  return CreateUserDto
}  
