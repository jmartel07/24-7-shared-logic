import { Exclude, Expose, Transform } from 'class-transformer'
import { existsSync } from 'fs'

export const getResponseUserDto = (ApiPropertySwagger?: any, picture_path?: string, picture_url?: string) => {
  // We did this to avoid having to include all nest dependencies related to ApiProperty on the client side too
  // With this approach the value of this decorator will be injected by the server but wont affect the client
  const ApiProperty = ApiPropertySwagger || function () { }

  @Exclude()
  class ResponseUserDto {
    @Expose()
    @ApiProperty()
    @Transform(({ value }) => Number(value), { toClassOnly: true })
    id: number

    @Expose()
    @ApiProperty()
    firstName: string

    @Expose()
    @ApiProperty()
    lastName: string

    @Expose()
    @ApiProperty()
    email: string

    @Expose()
    @ApiProperty({ required: false })
    position?: string

    @Expose()
    @ApiProperty({
      enum: [
        'COACH',
        'ATHLETE',
        'ACADEMY',
        'ACADEMY_ADMIN',
        'ADMIN',
      ]
    })
    userType: 'COACH' | 'ATHLETE' | 'ACADEMY' | 'ACADEMY_ADMIN' | 'ADMIN'
    @Expose()
    @ApiProperty({ required: false })
    @Transform(({ value }) => (value === '' ? undefined : new Date(value)))
    deactivatedAt?: Date

    @Expose()
    @Transform(({ value }) => {
      const filePath = [picture_path, value].join('')
      const url = [picture_url, value].join('')
      if (value && existsSync(filePath)) {
        return url
      }
    })
    @ApiProperty({ required: false })
    profilePicture?: string
  }

  return ResponseUserDto
}