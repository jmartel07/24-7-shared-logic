import { IsOptional } from 'class-validator'

export const getUpdateProfilePictureDto = (ApiPropertySwagger?: any) => {
  const ApiProperty = ApiPropertySwagger || function () { }

  class UpdateProfilePictureDto {
    @ApiProperty({
      required: false,
      type: 'file',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    })
    @IsOptional()
    profilePicture?: string

  }

  return UpdateProfilePictureDto
}
