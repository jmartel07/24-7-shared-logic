export enum UserType {
  COACH = 'COACH',
  ATHLETE = 'ATHLETE',
  ACADEMY = 'ACADEMY',
  ACADEMY_ADMIN = 'ACADEMY_ADMIN',
  ADMIN = 'ADMIN',
}

export enum PreferredFoot {
  LEFT = 'LEFT',
  RIGTH = 'RIGTH',
}

export enum SkillArea {
  ATTACKING = 'ATTACKING',
  DEFENDING = 'DEFENDING',
}

export enum SkillType {
  STRENGHT = 'STRENGHT',
  IMPROVEMENT = 'IMPROVEMENT',
}

export enum FormationArea {
  GOALKEEPER = 'GOALKEEPER',
  DEFENSE = 'DEFENSE',
  MIDFIELD = 'MIDFIELD',
  ATTACK = 'ATTACK',
}

export enum OvertakingArea {
  LOW = 'LOW',
  MEDIUM = 'MEDIUM',
  HIGH = 'HIGH',
}

export enum NotificationType {
  PENDING_ATHLETE_APPROVAL = 'PENDING_ATHLETE_APPROVAL',
  PENDING_EVALUATION_REVIEW = 'PENDING_EVALUATION_REVIEW',
}

export enum NotificationStatus {
  NEW = 'NEW',
  VIEWED = 'VIEWED',
  UNVIEWED = 'UNVIEWED',
}